bitbucket.org/zurichess/polyglot is a library which computes the polyglot keys
for chess positions.

See more about polyglot keys at http://hgm.nubati.net/book\_format.html
See documentation at https://godoc.org/bitbucket.org/zurichess/polyglot.

